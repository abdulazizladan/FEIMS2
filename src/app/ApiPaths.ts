export enum ApiPaths{
    signin = 'auth/login',
    signup = 'auth/register'
}