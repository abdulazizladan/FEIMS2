import { Component, OnInit, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

//Auth service
import { AuthService } from '../../services/auth.service';

//Material modules
import { MatButton } from '@angular/material/button';
import { MatProgressBar } from '@angular/material/progress-bar';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { map, takeUntil, tap } from 'rxjs/operators';
import { AppLoaderService } from '../../../shared/services/app-loader/app-loader.service';
import { JwtAuthService } from '../../../shared/services/auth/jwt-auth.service';
import { Store } from '@ngrx/store';
import { AppState } from 'app/store/app.reducer';
import { loginSuccess } from 'app/store/auth/auth.actions';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(MatProgressBar) progressBar: MatProgressBar;
  @ViewChild(MatButton) submitButton: MatButton;

  signinForm: FormGroup;
  errorMsg = '';

  private _unsubscribeAll: Subject<any>;

  constructor(
    private jwtAuth: JwtAuthService,
    private egretLoader: AppLoaderService,
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<AppState>,
    private auth: AuthService
  ) { 
    this._unsubscribeAll = new Subject();
  }

  /**
   * Init lifecycle hook
   */
  ngOnInit(): void {
    this.signinForm = new FormGroup({
      //Placeholder
      //email: new FormControl('shamskhalil@gmail.com', Validators.required),
      //password: new FormControl('shamsnet', Validators.required),
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      //rememberMe: new FormControl(true)
    });
  }

  /**
   * After view lifecycle hook
   */
  ngAfterViewInit() {
    this.autoSignIn();
  }

  /**
   * On Destroy lifecycle hook
   */
  ngOnDestroy() {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  /**
   * Signin
   */
  signin() {

    this.submitButton.disabled = true;
    this.progressBar.mode = 'indeterminate';
    
    this.auth.login(this.signinForm.value).pipe(
      map(res => res.payload),
      tap(({token, user}) => {
        this.store.dispatch(loginSuccess({token, user}))
      })
    ).subscribe(
      res => {
        this.router.navigateByUrl('dashboard')
      },
      err => {
        //console.log(err)
        this.submitButton.disabled = false;
        this.errorMsg = "Unable to login. Please check credentials and try again."
      }
    );
    
    //this.jwtAuth.signin(signinData.username, signinData.password)
    //  .subscribe(response => {
    //    this.router.navigateByUrl(this.jwtAuth.return);
    //  }, err => {
    //    this.submitButton.disabled = false;
    //    this.progressBar.mode = 'determinate';
    //    this.errorMsg = err.message;
    //    // console.log(err);
    //  })
  }

  /** 
   * Automatic signin 
  */
  autoSignIn() {
    if (this.jwtAuth.return === '/') {
      return
    }
    this.egretLoader.open(`Automatically Signing you in! \n Return url: ${this.jwtAuth.return.substring(0, 20)}...`, { width: '320px' });
    setTimeout(() => {
      this.signin();
      console.log('autoSignIn');
      this.egretLoader.close()
    }, 5000);
  }

}
